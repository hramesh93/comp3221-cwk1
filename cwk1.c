//
// COMP3221 Parallel Computation: OpenMP.
//

//
// Includes.
//

// Standard includes.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// The OMP library.
#include <omp.h>

// The include file for this coursework. You may inspect this file, but should NOT alter it, as it will be replaced
// with a different version for assessment.
#include "cwk1_extra.h"

// Key variables and functions in "cwk1_extra.h":
// - each entry in the data array is one of the following structs:
//   typedef struct Entry
//   {
//     char *name;
//     int id;
//   } Entry_t;
// - the pointer to the array, and the number of entries within it, are global:
//  Entry_t *orderedData;
//  int dataSize;
//
// int loadOrderedData() loads the data from "orderedData.txt" and returns 0 for success.
// void printData() displays the full data array to stdout.
// void deleteOrderedData() performs all required operations on the data array before quitting program.
//
// void swapEntries( int i, int j )
// - swaps the entries with indices i and j. Prints an error message if i==j, or if either index is out of range.
//   Uses local temporary variables. Not thread safe.
//
// int randomEntryIndex()
// - returns a random entry index, i.e. a random integer in the range 0 to dataSize-1 inclusive. Not thread safe.

//
// The following 4 functions correspond to each of the operations. It is expected you will add fill in each of these functions
// with your solution. You can also add other functions to this file if you like, but if you decide to add a new file, make
// sure to follow the coursework instructions for submission.
//

// Reverses the order of the data in-place, i.e. leaving the answer in the same array as before.
void reverseOrder_inParallel()
{
    int i, j;
#pragma omp parallel for private(i)
    for (i = 0; i < dataSize / 2; i++)
    {
        j = dataSize - (i + 1);
        swapEntries(i, j);
    }
}

// Sort all entries in-place, in order of increasing id.
void sortByID_inParallel()
{
    int i, j;

    for (i = 0; i < dataSize - 1; i++)
    {
        if (i % 2 == 0)
        {
#pragma omp parallel for private(j)
            for (j = 0; j < (dataSize / 2); j++)
            {
                if (orderedData[2 * j].id > orderedData[2 * j + 1].id && (2 * j + 1) < dataSize)
                {
                    // printf("%d %d\n", 2*j, (2*j)+1);
                    swapEntries((2 * j), (2 * j) + 1);
                }
            }
        }
        else
        {
#pragma omp parallel for private(j)
            for (j = 0; j < (dataSize / 2); j++)
            {
                if (orderedData[(2 * j) + 1].id > orderedData[(2 * j) + 2].id && (2 * j + 2) < dataSize)
                {
                    // printf("%d %d\n", (2*j)+1, (2*j)+2);
                    swapEntries((2 * j) + 1, (2 * j) + 2);
                }
            }
        }
    }
}

// Shuffle all items in parallel. Here, "shuffle" means to randomly select 2 entry indices, and swap the corresponding entries.
// This should be done dataSize*(dataSize-1)/2 times so that each pairing is swapped on average once.
void shuffle_inParallel()
{
    int k;
    #pragma omp parallel for private(k)
    for (k = 0; k < dataSize * (dataSize - 1) / 2; k++)
    {
        int i, j;
        i = randomEntryIndex();
        j = randomEntryIndex();

        if (i != j)
        {
            #pragma omp critical
            {
                swapEntries(i, j);
            }
        }
    }
}

// Remove the last item from the list in a thread-safe manner. You do not need to re-allocate any memory for the data array.
void removeLastItem_threadSafe()
{
    #pragma omp atomic
    dataSize--;
}

//
// You should not modify the code in main(), but should understand how it works.
//
int main(int argc, char **argv)
{
    // Initialise the random number generator to the system clock.
    srand(time(NULL));

    //
    // Parse command line arguments. Requires an option number to be entered.
    //

    // Make sure we have exactly 1 command line argument (which, plus the executable name, means 'argc' should be exactly 2).
    if (argc != 2)
    {
        printf("Enter a single command line argument for the operation required:\n(1) Reverse the order.\n(2) Sort in order of increasing ID.\n");
        printf("(3) Shuffle.\n(4) Remove all items from the end in a parallel loop.\n");
        return EXIT_FAILURE;
    }

    // Convert to an option number, and ensure it is in the valid range. Note argv[0] is the executable name.
    int option = atoi(argv[1]);
    if (option <= 0 || option > 4)
    {
        printf("Option number '%s' invalid.\n", argv[1]);
        return EXIT_FAILURE;
    }

    // Display how many threads we are using, if only to confirm this is actually in parallel.
    printf("Performing option '%i' using %i OpenMP thread(s).\n\n", option, omp_get_max_threads());

    //
    // Initialise the data set.
    //

    // Loads the data from file. loadOrderedData() (defined in "cwk1_extra.h" returns a non-negative integer
    // if successful, otherwise it will display an error message and return a negative integer.
    if (loadOrderedData() < 0)
        return EXIT_FAILURE;

    // Print the initial ordered data to screen. printData() is defined in "cwk1_extra.h".
    printf("Before the operation:\n");
    printData();

    //
    // Perform an operation on the data depending on the option entered on the command line.
    //
    int i, initialDataSize = dataSize;

    switch (option)
    {
    case 1:
        reverseOrder_inParallel();
        break;

    case 2:
        sortByID_inParallel();
        break;

    case 3:
        shuffle_inParallel();
        break;

    case 4:
#pragma omp parallel for
        for (i = 0; i < initialDataSize; i++)
            removeLastItem_threadSafe();
        break;

    default:

        // Shouldn't be possible to reach here given the earlier checks.
        printf("Option '%i' not implememnted by the switch() statement.\n", option);
        return EXIT_FAILURE;
    }

    //
    // Print the data after the operation, then free up all resources and quit.
    //
    printf("\nAfter the operation:\n");
    printData();

    // You MUST call this function from "cwk1_extra.h" (which should be unmodifed) just prior to quitting your program.
    deleteOrderedData();

    return EXIT_SUCCESS;
}

// 21
// Alice 26612
// Bob 81726
// Carol 91736
// Derek 71635
// Elizabeth 82636
// Frederick 17354
// Gabriel 71634
// Harold 61434
// Isabelle 31893
// Jonathan 92783
// Kerry 63545
// Larry 27635
// Michelle 82745
// Norman 62531
// Ophelia 36151
// Paul 57261
// Rachel 21534
// Stephen 17361
// Tina 82735
// Vaughan 18371
// Zebedee 83746